#include <stdio.h>
#include <unistd.h>
#include "bbio.h"

void my_callback(int fd) {
	printf("Before flushing buffer for file descriptor %d\n", fd);
}

int main ()
{
  // initialize BBIO (looup wrapper symbols)
  BBIO_Init("/home/vsgirelli/dev/bbio/test/tmp", 1024);
  FILE * pFile;
  int i, j;
  char buffer[] = { 'x' , 'y' , 'z' };
  pFile = fopen ("myfile.txt", "w");
  // attach a callback to the file descriptor
  printf("print 1\n");
  BBIO_On_flush(fileno(pFile), my_callback);
  printf("print 2\n");

    BBIO_Enable(fileno(pFile));
  //for (i = 0; i < 1024; i++) {
    //fprintf(pFile, "%d", i);
    fwrite(buffer , sizeof(char), sizeof(buffer), pFile);
  printf("print 3\n");
    // the following write is buffered
    fwrite(buffer , sizeof(char), sizeof(buffer), pFile);
    //sleep(5);
  printf("print 4\n");
  sleep(30);
    BBIO_Disable(fileno(pFile));
  printf("print 5\n");
    // the following write is not buffered
    //printf("%d", i);
    fwrite(buffer , sizeof(char), sizeof(buffer), pFile);
    BBIO_Enable(fileno(pFile));
    // the following write is buffered
    fwrite(buffer , sizeof(char), sizeof(buffer), pFile);
    //fprintf(pFile, "%d", i);
    BBIO_Flush(fileno(pFile));
    // the following writes are buffered
    /*
    for(j = 0; j < 512; j++) {
      fprintf(pFile, "%d", i);
      fwrite(buffer , sizeof(char), sizeof(buffer), pFile);
    }*/
  //}
  // this triggers a flush
  //sleep(5);
  fclose(pFile);
  // finalize BBIO
  BBIO_Finalize();
  return 0;
}
