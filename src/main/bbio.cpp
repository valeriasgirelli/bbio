/*
 * Copyright (C) 2015 University of Chicago.
 * See COPYRIGHT notice in top-level directory.
 *
 */

#include <stdlib.h>
#include <dlfcn.h>
#include <iostream>
#include "bbio.h"

static int (*bbio_init_impl)(const char* path, size_t buf_size) = NULL;
static int (*bbio_finalize_impl)() 	= NULL;
static int (*bbio_enable_impl)(int) 	= NULL;
static int (*bbio_disable_impl)(int) 	= NULL;
static int (*bbio_flush_impl)(int) 	= NULL;
static int (*bbio_on_flush_impl)(int,BBIO_Callback) = NULL;

#define BBIO_LOAD(name) \
	*(void **) (&name ## _impl) = dlsym(handle, #name)

#define BBIO_UNLOAD(name) \
	name ## _impl = NULL

#define BBIO_CALL(name,...) \
	(name ## _impl != NULL) ? name ## _impl(__VA_ARGS__) : BBIO_ERROR

extern "C" int BBIO_Init(const char* path, size_t buf_size)
{
  std::cout << "BBIO\n";
	if(bbio_init_impl != NULL) return BBIO_ERROR;
	void *handle = dlopen(NULL, RTLD_NOW | RTLD_GLOBAL);
  *(void **) (&bbio_init_impl) = dlsym(handle, "bbio_init");
	//BBIO_LOAD(bbio_init);
	BBIO_LOAD(bbio_finalize);
	BBIO_LOAD(bbio_enable);
	BBIO_LOAD(bbio_disable);
	BBIO_LOAD(bbio_flush);
	BBIO_LOAD(bbio_on_flush);
  std::cout << "BBIO final\n";
	int ret = BBIO_CALL(bbio_init,path,buf_size);
  std::cout << "retorno: " << ret << "\n";
  return ret;
}

extern "C" int BBIO_Finalize()
{
	int err = BBIO_CALL(bbio_finalize);
	BBIO_UNLOAD(bbio_init);
	BBIO_UNLOAD(bbio_finalize);
	BBIO_UNLOAD(bbio_enable);
	BBIO_UNLOAD(bbio_disable);
	BBIO_UNLOAD(bbio_flush);
	BBIO_UNLOAD(bbio_on_flush);
	return err;
}

extern "C" int BBIO_Enable(int file)
{
	return BBIO_CALL(bbio_enable, file);
}

extern "C" int BBIO_Disable(int file)
{
	return BBIO_CALL(bbio_disable, file);
}

extern "C" int BBIO_Flush(int file)
{
	return BBIO_CALL(bbio_flush, file);
}

extern "C" int BBIO_On_flush(int fd, BBIO_Callback cb)
{
	return BBIO_CALL(bbio_on_flush,fd,cb);
}
