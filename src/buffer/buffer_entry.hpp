/*******************************************************************************
* File buffer_entry.hpp
*
* Buffering functions
* 
* Created June 15th 2015 - Anthony Kougkas
* (C) 2015 by Argonne National Laboratory.
******************************************************************************/
#ifndef BUFFER_ENTRY_HPP
#define BUFFER_ENTRY_HPP
/******************************************************************************
*standard include files 
******************************************************************************/
#include <stdio.h>
#include <cstring>
#include <math.h>
#include <vector>
#include <sys/uio.h>
#include <unistd.h>
#include <unordered_map>
#include <memory>
/******************************************************************************
*Class buffer entry
******************************************************************************/
namespace bbio {

/**
 * The BufferEntry object represents a buffer. Each BufferEntry instance is
 * associated with a file descriptor.
 */
class BufferEntry {

	private:

	static std::unordered_map<int, std::shared_ptr<bbio::BufferEntry>> pool;
	/*< pool of instanciated buffers */

	char* data; /*< pointer to the memory where the buffer is located */
	const int fd; /*< file descriptor of the file being buffered */
	size_t pos; /*< position of the first free byte in the buffer */
	size_t prv_pos; /*< position of the previous operation in the buffer */
	size_t file_offset; /*< current file pointer */
	const size_t max_size_; /*< size of the buffer */
	bool enabled_; /*< whether is enabled or not */
	int buf_fd; /*< file descriptor of the mmapped file that holds the data */
	BBIO_Callback cb; /*< function to call when the buffer is full */

	/**
	 * Constructor. Cannot be called externally; use BufferEntry::create instead.
	 */
	BufferEntry(int fh, const std::string& bufpath, size_t max_size);

	public:

	/**
	 * Destructor.
	 */
	~BufferEntry();

	int enable(bool v);

	bool enabled() const { return enabled_; }
	
/******************************************************************************
*Write function 
******************************************************************************/
	int write(const char* buf, size_t count);
/******************************************************************************
*Writev function 
******************************************************************************/
	int writev(const struct iovec* iov, size_t iovcnt);
/******************************************************************************
*Pwrite function 
******************************************************************************/
	int pwrite(const char* buf, size_t count, off64_t offset);
/******************************************************************************
*Seek function 
******************************************************************************/
	off64_t seek(off64_t offset, int whence);
/******************************************************************************
*Execute function (flashes the buffer) 
******************************************************************************/
	int execute();

	void set_callback(BBIO_Callback c) { cb = c; }

	static std::shared_ptr<bbio::BufferEntry> get(int fd);

	static int create(int fd, const std::string& bufpath, size_t max_size);

	static int remove(int fd);

	static int clear();
};

}
#endif /* BUFFER_ENTRY_HPP */
