/*******************************************************************************
* File buffer_entry.cpp
*
* Buffering functions
* 
* Created June 15th 2015 - Anthony Kougkas
* (C) 2015 by Argonne National Laboratory.
******************************************************************************/
/******************************************************************************
*standard include files 
******************************************************************************/
#include <sys/mman.h>
#include <iostream>

#include "bbio.h"
#include "buffer_entry.hpp"
#include "opcode.h"
/******************************************************************************
*some DEFINES
******************************************************************************/
#define BBIO_REAL(name,ret,args) \
	extern  ret (*__real_ ## name)args
  
extern "C" {
BBIO_REAL(write, ssize_t, (int fd, const void *buf, size_t count));
BBIO_REAL(pwrite64, ssize_t, (int fd, const void *buf, size_t count, off64_t offset));
BBIO_REAL(lseek64, off64_t, (int fd, off64_t offset, int whence));
BBIO_REAL(mkstemp, int, (const char* tpl));
BBIO_REAL(lseek, off_t, (int fd, off_t offset, int whence));
}
/******************************************************************************
*Class buffer entry
******************************************************************************/
namespace bbio {

BufferEntry::BufferEntry(int fh, const std::string& bufpath, size_t max_size) 
: data(NULL), fd(fh), pos(0), prv_pos(0), file_offset(0), max_size_(max_size), enabled_(BBIO_TRUE), buf_fd(0)
{
  std::cout << "Trying to create file\n";
	cb = NULL;
	if(bufpath.size() == 0) {
		data = (char*)mmap(NULL, max_size_, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
	} else {
		std::string tpl = bufpath+"/bbio_XXXXXX";
    // creates a regular file derived from tpl and returns the file descriptor
		buf_fd = __real_mkstemp((char*)tpl.c_str());
    // truncates the regular file specified by buf_fd to a size of max_size_
		ftruncate(buf_fd, max_size_);
    // deletes a name from the file system. The file is kept until there are any file descriptors referring to it
		//unlink(tpl.c_str());
    // mmap returns the mem address mapped, and it's directly accessed inside the program
		data = (char*)mmap(NULL, max_size_, PROT_READ | PROT_WRITE, MAP_SHARED, buf_fd, 0);
	}
}

BufferEntry::~BufferEntry() {
	execute();
	munmap(data,max_size_);
	if(buf_fd != 0) close(buf_fd);
}

int BufferEntry::enable(bool v) {
	enabled_ = v;
	return BBIO_OK;
}

/******************************************************************************
*Write function 
******************************************************************************/
int BufferEntry::write(const char* buf, size_t count)
{
	size_t op_size = 0;
	bool aggregate = (pos != prv_pos) && (data[prv_pos] == BBIO_WRITE);

	if(aggregate)
		op_size = count;
	else
		op_size = sizeof(char)+sizeof(count)+count;

	// not enough space in the buffer, flush it
	if((pos+op_size) > max_size_)
	{
		execute();
		aggregate = false;
	}
	// still not enough space to hold operation, don't buffer it
	if((pos+op_size) > max_size_)
	{
		return BBIO_ERROR;
	}
	// enough space, buffer the operation
	if(not aggregate) {
		data[pos]=BBIO_WRITE;
		std::memcpy(&data[pos+sizeof(char)], &count, sizeof(count));
		std::memcpy(&data[pos+sizeof(char)+sizeof(count)], buf, count);
		prv_pos = pos;
	} else {
		size_t xtra_count=0;
		std::memcpy(&xtra_count, &data[prv_pos+sizeof(char)], sizeof(count));
		xtra_count+=count;
		std::memcpy(&data[prv_pos+sizeof(char)], &xtra_count, sizeof(count));
		std::memcpy(&data[pos], buf, count);
	}
	pos += op_size;
	file_offset += count;
	return BBIO_OK;
}
/******************************************************************************
*Writev function 
******************************************************************************/
int BufferEntry::writev(const struct iovec* iov, size_t iovcnt)
{
	size_t op_size = sizeof(char)+sizeof(size_t);
	for(unsigned i=0; i<iovcnt; i++) op_size += iov[i].iov_len;

	if((pos+op_size) > max_size_) execute();
	if((pos+op_size) > max_size_) return BBIO_ERROR;

	for (unsigned i=0; i<iovcnt;i++){
		write((const char*)(iov[i].iov_base), iov[i].iov_len);
	}
	return BBIO_OK;
}
/******************************************************************************
*Pwrite function 
******************************************************************************/
int BufferEntry::pwrite(const char* buf, size_t count, off64_t offset)
{
	size_t op_size = sizeof(char)+sizeof(count)+count+sizeof(offset);
	// not enough space, flush the buffer
	if((pos+op_size) > max_size_) {
		execute();
	}
	// still not enough space, execute pwrite normally
	if((pos+op_size) > max_size_) {
		return BBIO_ERROR;
	}
	// enough space, do the buffering
	data[pos]=BBIO_PWRITE;
	std::memcpy(&data[pos+sizeof(char)], &count, sizeof(count));
	std::memcpy(&data[pos+sizeof(char)+sizeof(count)], &offset, sizeof(offset));
	std::memcpy(&data[pos+sizeof(char)+sizeof(count)+sizeof(offset)], buf, count);
	pos+=op_size;
	return BBIO_OK;
}
/******************************************************************************
*Seek function 
******************************************************************************/
off64_t BufferEntry::seek(off64_t offset, int whence)
{
	if(whence==SEEK_END)
	{
		int err = execute();
		if(err) return -1;
		file_offset = (size_t)__real_lseek64(fd,offset,whence);
	}
	else
	{
		if (whence==SEEK_SET) file_offset = offset;
		else if(whence==SEEK_CUR) file_offset += offset;

		size_t op_size = sizeof(char)+sizeof(off64_t)+sizeof(int);
		if((pos+op_size) > max_size_)
		{
			execute();
		}
		if((pos+op_size) > max_size_) {
			return -2;
		}
			
		data[pos] = BBIO_SEEK;
		std::memcpy(&data[pos+sizeof(char)], &offset, sizeof(offset));
		std::memcpy(&data[pos+sizeof(char)+sizeof(offset)], &whence, sizeof(whence));

		prv_pos=pos;
		pos+=op_size;
	}
	return (off64_t)file_offset;
}
/******************************************************************************
*Execute function (flushes the buffer) 
******************************************************************************/
int BufferEntry::execute()
{
	if (pos==0) return BBIO_OK;

	if(cb != NULL) cb(fd);
	
	size_t v_pos = 0;
	size_t pos_buf,pos_cnt,pos_off, pos_whence;
	size_t count;
	off64_t offset;
	int whence;
		
	while(v_pos < pos)
	{
		switch(data[v_pos])
		{
			case BBIO_WRITE:
				pos_cnt = v_pos+sizeof(char);
				std::memcpy(&count, &data[pos_cnt], sizeof(size_t));
				pos_buf = pos_cnt + sizeof(size_t);
				__real_write(fd, &data[pos_buf], count);
				v_pos = pos_buf + count;
				break;
			case BBIO_PWRITE:
				pos_cnt = v_pos+sizeof(char);
				std::memcpy(&count, &data[pos_cnt], sizeof(size_t));
				pos_off = pos_cnt + sizeof(count);
				std::memcpy(&offset, &data[pos_off], sizeof(off64_t));
				pos_buf = pos_off+sizeof(offset);
				__real_pwrite64(fd, &data[pos_buf], count, offset);
				v_pos = pos_buf + count;
				break;
			case BBIO_SEEK:
				pos_off = v_pos+sizeof(char);
				std::memcpy(&offset, &data[pos_off], sizeof(off64_t));
				pos_whence = pos_off + sizeof(offset);
				std::memcpy(&whence, &data[pos_whence], sizeof(int));
				__real_lseek64(fd,offset,whence);
				v_pos=pos_whence+sizeof(int);
				break;
		}
	}

	pos=0;
	prv_pos=0;
	return BBIO_OK;
}

std::unordered_map<int, std::shared_ptr<bbio::BufferEntry>> BufferEntry::pool;

std::shared_ptr<bbio::BufferEntry> BufferEntry::get(int fd)
{
	if(pool.count(fd) == 0) return std::shared_ptr<bbio::BufferEntry>();
	return pool[fd];
}

int BufferEntry::create(int fd, const std::string& bufpath, size_t max_size)
{
	if(pool.count(fd) != 0) return BBIO_ERROR;
	pool[fd] = std::shared_ptr<BufferEntry>(new BufferEntry(fd,bufpath,max_size));
	return BBIO_OK;
}

int BufferEntry::remove(int fd)
{
	if(pool.count(fd) == 0) return BBIO_ERROR;
	pool.erase(fd);
	return BBIO_OK;
}

int BufferEntry::clear()
{
	pool.clear();
	return BBIO_OK;
}
	
}
