/*
 * Copyright (C) 2015 University of Chicago.
 * See COPYRIGHT notice in top-level directory.
 *
 */

#ifndef BBIO_BUFFER_MACROS_H
#define BBIO_BUFFER_MACROS_H
/******************************************************************************
*standard include files 
******************************************************************************/
#include "bbio-config.h"
#include "bbio.h"
#include "buffer.h"
#include <stdio.h>
#include <sys/uio.h>

/******************************************************************************
*Macros
******************************************************************************/
#define bbio_creat_begin(path,mode)	\
	MAP_OR_FAIL(mkstemp); \
	MAP_OR_FAIL(lseek); \
	int buffered = bbio_buffer_create_or_open_begin(path) 
#define bbio_creat_end(ret) 	\
	if(buffered) bbio_buffer_create_or_open_end(ret)

#define bbio_creat64_begin(path,mode)	\
	MAP_OR_FAIL(mkstemp); \
	MAP_OR_FAIL(lseek); \
	int buffered = bbio_buffer_create_or_open_begin(path)
#define bbio_creat64_end(ret)	\
	if(buffered) bbio_buffer_create_or_open_end(ret)

#define bbio_open_begin(path, flag, ...)	\
	MAP_OR_FAIL(mkstemp); \
	MAP_OR_FAIL(lseek); \
	int buffered = bbio_buffer_create_or_open_begin(path)
#define bbio_open_end(ret)	\
	if(buffered) bbio_buffer_create_or_open_end(ret)

#define bbio_open64_begin(path, flag, ...)	\
	MAP_OR_FAIL(mkstemp); \
	MAP_OR_FAIL(lseek); \
	int buffered = bbio_buffer_create_or_open_begin(path)
#define bbio_open64_end(ret)	\
	if(buffered) bbio_buffer_create_or_open_end(ret)

#define bbio_close_begin(fd)	\
	bbio_buffer_close_or_sync_begin(fd,BBIO_TRUE)
#define bbio_close_end(ret)

#define bbio_fopen_begin(path,mode)	\
	MAP_OR_FAIL(mkstemp); \
	MAP_OR_FAIL(lseek); \
	int buffered = bbio_buffer_create_or_open_begin(path) 
#define bbio_fopen_end(ret)	\
	if(buffered && ret) { setvbuf(ret, NULL, _IONBF, 0); \
	bbio_buffer_create_or_open_end(fileno(ret)); }

#define bbio_fopen64_begin(path,mode)	\
	MAP_OR_FAIL(mkstemp); \
	MAP_OR_FAIL(lseek); \
	int buffered = bbio_buffer_create_or_open_begin(path)
#define bbio_fopen64_end(ret)	\
	if(buffered) { setvbuf(ret, NULL, _IONBF, 0); \
	bbio_buffer_create_or_open_end(fileno(ret)); }

#define bbio_fclose_begin(file)	\
	bbio_buffer_close_or_sync_begin(fileno(file),BBIO_TRUE)
#define bbio_fclose_end(ret)

#define bbio_mkstemp_begin(temp)
#define bbio_mkstemp_end(ret)

#define bbio_mkostemp_begin(temp,flags)
#define bbio_mkostemp_end(ret)

#define bbio_mkstemps_begin(temp,suffixlen)
#define bbio_mkstemps_end(ret)

#define bbio_mkostemps_begin(temp,suffixlen,flags)
#define bbio_mkostemps_end(ret)	

#define bbio_write_begin(fh,buf,count)		\
	MAP_OR_FAIL(write);				\
	int buffered = bbio_buffer_write_begin(fh, buf, count); 	\
	if(!buffered) {
#define bbio_write_end(ret)	} else ret=count; 

#define bbio_read_begin(fh,buf,count) \
	bbio_flush(fd)
#define bbio_read_end(ret)

#define bbio_writev_begin(fh,iov,iovcnt)		\
	MAP_OR_FAIL(write);				\
	int buffered=bbio_buffer_writev_begin(fh, iov,iovcnt); 	\
	if(!buffered) {
#define bbio_writev_end(ret)	} else {\
	ret=0; int _i; for(_i=0; _i<iovcnt; _i++) ret += iov[_i].iov_len; }

#define bbio_readv_begin(fh,iov,iovcnt) \
	bbio_flush(fd)
#define bbio_readv_end(ret)

#define bbio_fwrite_begin(buf,size,nmemb,file)			\
	MAP_OR_FAIL(write); 						\
	int buffered=bbio_buffer_write_begin(fileno(file), buf, (nmemb)*(size)); 	\
	if(!buffered) {
#define bbio_fwrite_end(ret)	} else ret=(nmemb)*(size);

#define bbio_fread_begin(buf,size,nmemb,file) \
	bbio_flush(fileno(file))
#define bbio_fread_end(ret)

#define bbio_pwrite_begin(fh,buf,count,offset)	\
	MAP_OR_FAIL(pwrite64);				\
	int buffered=bbio_buffer_pwrite_begin(fh,buf,count,offset);\
	if(!buffered) {
#define bbio_pwrite_end(ret)	} else ret=count;

#define bbio_pwrite64_begin(fh,buf,count,offset)	\
	MAP_OR_FAIL(pwrite64);				\
	int buffered=bbio_buffer_pwrite_begin(fh,buf,count,offset);\
	if(!buffered) {
#define bbio_pwrite64_end(ret)	} else ret=count;

#define bbio_pread_begin(fh,buf,count,offset) \
	bbio_flush(fh)
#define bbio_pread_end(ret)

#define bbio_pread64_begin(fh,buf,count,offset) \
	bbio_flush(fh)
#define bbio_pread64_end(ret)

#define bbio_lseek_begin(fh,offset,whence)			\
	MAP_OR_FAIL(lseek64);					\
	ret = bbio_buffer_seek_begin(fh, offset, whence); 	\
	if(ret==-2) {
#define bbio_lseek_end(ret)	};

#define bbio_lseek64_begin(fh,offset,whence)			\
	MAP_OR_FAIL(lseek64);					\
	ret = bbio_buffer_seek_begin(fh, offset, whence); 	\
	if(ret==-2) {
#define bbio_lseek64_end(ret)	}; 

#define bbio_fseek_begin(fh,offset,whence)	\
	MAP_OR_FAIL(lseek64);	\
	off64_t __offset = bbio_buffer_seek_begin(fileno(fh), offset, whence); \
	if(__offset==-2) {
#define bbio_fseek_end(ret)	} else ret = (__offset >= 0);

#define bbio_fsync_begin(fh)	\
	bbio_buffer_close_or_sync_begin(fh,BBIO_FALSE)
#define bbio_fsync_end(ret)

#define bbio_fdatasync_begin(fh) \
	bbio_buffer_close_or_sync_begin(fh,BBIO_FALSE)
#define bbio_fdatasync_end(ret)

#define bbio_fxstat_begin(v,fd,stt) \
	bbio_flush(fd)
#define bbio_fxstat_end(ret)

#define bbio_fxstat64_begin(v,fd,stt) \
	bbio_flush(fd)
#define bbio_fxstat64_end(ret)

#define bbio_lxstat_begin(v,path,stt)
#define bbio_lxstat_end(ret)

#define bbio_lxstat64_begin(v,path,stt)
#define bbio_lxstat64_end(ret)

#define bbio_xstat_begin(ver,path,stt)
#define bbio_xstat_end(ret)

#define bbio_xstat64_begin(ver,path,stt)
#define bbio_xstat64_end(ret)

#define bbio_mmap_begin(addr,length,prot,flags,fd,offset)
#define bbio_mmap_end(ret)

#define bbio_mmap64_begin(addr,length,prot,flags,fd,offset)
#define bbio_mmap64_end(ret)

#define bbio_aio_read_begin(aiocbp)
#define bbio_aio_read_end(ret)

#define bbio_aio_read64_begin(aiocbp)
#define bbio_aio_read64_end(ret)

#define bbio_aio_write_begin(aiocbp)
#define bbio_aio_write_end(ret)

#define bbio_aio_write64_begin(aiocbp)
#define bbio_aio_write64_end(ret)

#define bbio_aio_return_begin(aiocbp)
#define bbio_aio_return_end(ret)

#define bbio_aio_return64_begin(aiocbp)
#define bbio_aio_return64_end(ret)

#define bbio_lio_listio_begin(mode,aiocb_list,nitems,sevp)
#define bbio_lio_listio_end(ret)

#define bbio_lio_listio64_begin(mode,aiocb_list,nitems,sevp)
#define bbio_lio_listio64_end(ret)

#endif /* IOLOGY_BUFFER_H */
