/*******************************************************************************
* File buffer.cpp
*
* Buffering operations
* 
* Created June 12th 2015 - Anthony Kougkas
* (C) 2015 by Argonne National Laboratory.
******************************************************************************/

/******************************************************************************
*standard include files 
******************************************************************************/
#include "bbio.h"
#include "buffer_entry.hpp"
#include <memory>
#include <iostream>
#include <stdio.h>

namespace bbio {

	size_t max_buf_size;
	std::string buf_path;
}

extern "C" int bbio_init(const char* path, size_t buf_size) {
  //std::cout << "bbio init\n";
	bbio::max_buf_size = buf_size;
	if(path != NULL) bbio::buf_path = std::string(path);
	return BBIO_OK;
}

extern "C" int bbio_finalize() {
	bbio::BufferEntry::clear();
	return BBIO_OK;
}

extern "C" int bbio_enable(int fd) {
	auto entry = bbio::BufferEntry::get(fd);
	if(not entry) {
		bbio::BufferEntry::create(fd,bbio::buf_path,bbio::max_buf_size);
		entry = bbio::BufferEntry::get(fd);
	}
	entry->enable(BBIO_TRUE);
	return BBIO_OK;
}

extern "C" int bbio_disable(int fd) {
	auto entry = bbio::BufferEntry::get(fd);
	if(entry) {
		entry->execute();
		entry->enable(BBIO_FALSE);
	}
	return BBIO_OK;
}

extern "C" int bbio_flush(int fd) {
	auto entry = bbio::BufferEntry::get(fd);
	if(entry) {
		entry->execute();
		return BBIO_OK;
	}
	return BBIO_ERROR;
}

extern "C" int bbio_on_flush(int fd, BBIO_Callback cb) {
	auto entry = bbio::BufferEntry::get(fd);
	if(entry) {
		entry->set_callback(cb);
		return BBIO_OK;
	}
	return BBIO_ERROR;
}

/******************************************************************************
*Misc function definitions
******************************************************************************/
static bool is_excluded(const char* path) {
	static const char* excluded[] = {
		"/etc/", "/dev/", "/usr/", "/bin/", "/boot/", 
		"/lib/", "/opt/", "/sbin/", "/sys/", "/proc/",
		NULL
	};
	int tmp_index = 0;
	const char* exclude;
	while((exclude = excluded[tmp_index])) {
		if(!(strncmp(exclude, path, strlen(exclude)))) break;
		tmp_index++;
	}
	return exclude != 0;
}
/******************************************************************************
*Function definitions
******************************************************************************/
extern "C" int bbio_buffer_create_or_open_begin(const char* path)
	{
		if(is_excluded(path)) return BBIO_FALSE;
		return BBIO_TRUE;
	}


extern "C" int bbio_buffer_create_or_open_end(int ret)
	{
		bbio::BufferEntry::create(ret,bbio::buf_path,bbio::max_buf_size);
		return BBIO_OK;
	}



extern "C" int bbio_buffer_write_begin(int fd, const char* buf, size_t count)
	{
		std::shared_ptr<bbio::BufferEntry> entry = bbio::BufferEntry::get(fd);
		if(entry)
		{
			if(entry->enabled() == BBIO_TRUE)
			{
				int err = entry->write(buf, count);
				if(err == BBIO_ERROR) return BBIO_FALSE;
			}
			else 
			{
				entry->execute();
				return BBIO_FALSE;
			}
		}
		else return BBIO_FALSE;
		return BBIO_TRUE;
	}

extern "C" int bbio_buffer_writev_begin(int fd, const struct iovec* iov, size_t iovcnt)
	{
		std::shared_ptr<bbio::BufferEntry> entry = bbio::BufferEntry::get(fd);
		if(entry)
		{
			if(entry->enabled() == BBIO_TRUE)
			{
				int err = entry->writev(iov, iovcnt);
				if(err == BBIO_ERROR) return BBIO_FALSE;
			}
			else
			{
				entry->execute();
			}
		}
		else return BBIO_FALSE;
		return BBIO_TRUE;
	}

extern "C" int bbio_buffer_pwrite_begin(int fd, const char *buf, size_t count, off64_t offset)
	{
		std::shared_ptr<bbio::BufferEntry> entry = bbio::BufferEntry::get(fd);
		if(entry)
		{
			if (entry->enabled() == BBIO_TRUE)
			{
				
				int err = entry->pwrite(buf, count, offset);
				if(err == BBIO_ERROR) return BBIO_FALSE;
			}
			else
			{
				entry->execute();
			}
		}
		else return BBIO_FALSE;
		return BBIO_TRUE;
	}

extern "C" off64_t bbio_buffer_seek_begin(int fd, off64_t offset, int whence)
	{
		std::shared_ptr<bbio::BufferEntry> entry = bbio::BufferEntry::get(fd);
		if(entry)
		{
			if (entry->enabled() == BBIO_TRUE)
			{
				off64_t _offset = entry->seek(offset, whence);
				if(_offset==-2)
				{
					entry->execute();
					_offset = entry->seek(offset, whence);
				}
				if(_offset<=-1) return -1;
				return _offset;
			}
			else 
			{
				entry->execute();
			}
			
		}
		else return -2;
		return -2;
	}


extern "C" int bbio_buffer_close_or_sync_begin(int fd, int remove) 
	{
		std::shared_ptr<bbio::BufferEntry> entry = bbio::BufferEntry::get(fd);
		if(entry)
		{
			int err = entry->execute();
			if(remove) bbio::BufferEntry::remove(fd);
			if(err == BBIO_ERROR)
			{
				// TODO set an error code or something...
				return BBIO_FALSE;
			}
		}
		else return BBIO_FALSE;
		return BBIO_TRUE;
	}
